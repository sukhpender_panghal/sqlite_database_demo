package com.example.sqlite_demo.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.EditText
import com.example.sqlite_demo.R
import com.example.sqlite_demo.model.Data
import kotlinx.android.synthetic.main.custom_row_layout.view.*

class ListPersonAdapter(internal var activity: Activity,
                        internal var istperson:List<Data>
                        ,internal val edt_id:EditText,
                        internal val edt_name:EditText,
                        internal val edt_address:EditText):BaseAdapter() {
    internal val inflater:LayoutInflater
        init {
         inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

    @SuppressLint("ViewHolder")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val rowView:View
        rowView = inflater.inflate(R.layout.custom_row_layout,null)

        rowView.txt_row_id.text = istperson[p0].id.toString()
        rowView.txt_row_name.text = istperson[p0].name.toString()
        rowView.txt_row_address.text = istperson[p0].addresss.toString()

        rowView.setOnClickListener {
            edt_id.setText(istperson[p0].id.toString())
            edt_name.setText(istperson[p0].name.toString())
            edt_address.setText(istperson[p0].addresss.toString())

        }
        return rowView
    }

    override fun getItem(p0: Int): Any {
        return istperson[p0]
    }

    override fun getItemId(p0: Int): Long {
        return istperson[p0].id.toLong()
    }

    override fun getCount(): Int {
        return istperson.size
    }
}