package com.example.sqlite_demo

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Build
import androidx.annotation.RequiresApi
import com.example.sqlite_demo.model.Data


class DBHelper(context: Context) : SQLiteOpenHelper(context,DATABASE_NAME,null,DATABASE_VER){

    companion object{
        private var DATABASE_VER = 1
        private val DATABASE_NAME = "DataBase1"

        private val TABLE_NAME = "persons"
        private val COL_ID = "Id"
        private val COL_NAME = "Name"
        private val COL_ADDRESS = "Address"
    }
    override fun onCreate(p0: SQLiteDatabase?) {
        val create_table = ("CREATE TABLE $TABLE_NAME($COL_ID INTEGER PRIMARY KEY,$COL_NAME TEXT,$COL_ADDRESS TEXT)")
        p0!!.execSQL(create_table)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        p0!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
    }

    val allPerson:List<Data>
    get() {
        val istPerson = ArrayList<Data>()
        val selectQuery = "SELECT * FROM $TABLE_NAME"
        val db = this.writableDatabase
        val cursor = db.rawQuery(selectQuery,null)
        if (cursor.moveToFirst()){
            do {
                val person = Data()
                person.id = cursor.getInt(cursor.getColumnIndex(COL_ID))
                person.name = cursor.getString(cursor.getColumnIndex(COL_NAME))
                person.addresss = cursor.getString(cursor.getColumnIndex(COL_ADDRESS))

                istPerson.add(person)
            }while (cursor.moveToFirst())
        }
        db.close()
        return istPerson
    }

    @RequiresApi(Build.VERSION_CODES.P)
    fun addPerson(person: Data){
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID,person.id)
        values.put(COL_NAME,person.name)
        values.put(COL_ADDRESS,person.addresss)

        db.insert(TABLE_NAME,null,values)
        db.close()
    }
    fun updatePerson(person: Data):Int{

        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID,person.id)
        values.put(COL_NAME,person.name)
        values.put(COL_ADDRESS,person.addresss)

        return db.update(TABLE_NAME,values,"$COL_ID=?", arrayOf(person.id.toString()))
    }
    fun deletePerson(person: Data){

        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID,person.id)
        values.put(COL_NAME,person.name)
        values.put(COL_ADDRESS,person.addresss)

        db.delete(TABLE_NAME,"$COL_ID=?", arrayOf(person.id.toString()))
        db.close()
    }
}