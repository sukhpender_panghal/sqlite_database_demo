package com.example.sqlite_demo

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import com.example.sqlite_demo.adapter.ListPersonAdapter
import com.example.sqlite_demo.model.Data
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

     var db = DBHelper(this)
            var istPerson:List<Data> = ArrayList()
    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = DBHelper(this)
        refreshData()

        btn_add.setOnClickListener {
            val person = Data(edt_id.text.toString().toInt(),
                edt_name.text.toString(),
                edt_address.text.toString()
            )
            db.addPerson(person)
            refreshData()
        }
        btn_update.setOnClickListener {

            val person = Data(edt_id.text.toString().toInt(),
                edt_name.text.toString(),
                edt_address.text.toString()
            )
            db.updatePerson(person)
            refreshData()
        }
        btn_delete.setOnClickListener {
            val person = Data(edt_id.text.toString().toInt(),
                edt_name.text.toString(),
                edt_address.text.toString()
            )
            db.deletePerson(person)
            refreshData()
        }
    }

    private fun refreshData() {
        istPerson = db.allPerson
        val adapter1 = ListPersonAdapter(this, istPerson, edt_id, edt_name, edt_address)
        list_enteries.adapter = adapter1
    }
}